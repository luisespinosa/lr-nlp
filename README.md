# NAACL 2018 Tutorial on The Interplay between Lexical Resources and NLP
- Jose Camacho-Collados, Luis Espinosa-Anke and Mohammad Taher Pilehvar

Welcome to the official repository of the NAACL 2018 Tutorial on _The Interplay between Lexical Resources and NLP_.

## Abstract

Incorporating linguistic, world and common sense knowledge into AI/NLP systems is currently an important research area, with several open problems and challenges. At the same time, processing and storing this knowledge in lexical resources is not a straightforward task. This tutorial proposes to address these complementary goals from two methodological perspectives: the use of NLP methods to help the process of constructing and enriching lexical resources and the use of lexical resources for improving NLP applications.
Two main types of audience can benefit from this tutorial: those working on language resources who are interested in becoming acquainted with automatic NLP techniques, with the end goal of speeding and/or easing up the process of resource curation; and on the other hand, researchers in NLP who would like to benefit from the knowledge of lexical resources to improve their systems and models.

## Resources

A discussion forum is available as a Google Group at https://groups.google.com/forum/#!forum/naacl2018tutorial-interplay-resources-nlp

The slides are available from [here](./slides/NAACL2018_Tutorial_ Interplay_LexicalResources-NLP.pdf) and an outline with all the references [here](https://arxiv.org/pdf/1807.00571.pdf).

